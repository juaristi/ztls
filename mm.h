/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef _MM_H_
#define _MM_H_
#include <malloc.h>

#define Z_MALLOC_STYLE __attribute__((malloc))

void mm_strdup(char **, const char *);

#define mm_new0(type) mm_malloc0(sizeof(type))
void *mm_malloc0(size_t) Z_MALLOC_STYLE;

static inline void mm_free(void *ptr)
{
	if (ptr)
		free(ptr);
}

#define MM_FREE2(ptr) mm_free2((void **) ptr)

static inline void mm_free2(void **ptr)
{
	if (ptr && *ptr) {
		free(*ptr);
		*ptr = NULL;
	}
}

#endif
