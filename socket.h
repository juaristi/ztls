/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef _SOCKET_H_
#define _SOCKET_H_
#include <stdio.h>
#include <stdint.h>
#include <picotls.h>

typedef uint16_t socket_state_t;

typedef enum {
	ACCEPT_FD,
	CLIENT_SOCKET_FD
} socket_type_t;

typedef struct {
	int fd;
	socket_type_t type;
	socket_state_t state;
	uint8_t *out;
	size_t outlen;

	ptls_t *ptls;
} z_socket_t;

/*
 * States
 * These are the possible values for socket_state_t.
 * Some of these can be specified simultaneously, making a bit field.
 */
#define RECV_CLIENT_HELLO	0x0
#define OPAQUE_IN_PROGRESS	0x0
#define FINISHED		0x1
#define WRITE_PENDING		0x2
#define INTERRUPTED		0x4
#define FAILED			0xffff

void z_append(z_socket_t *, const uint8_t *, size_t);

void z_try_flush(z_socket_t *);
void z_write(z_socket_t *, const uint8_t *, size_t);

#endif
