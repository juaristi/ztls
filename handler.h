/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef _HANDLER_H_
#define _HANDLER_H_

struct z_backend_st;
typedef struct z_backend_st z_backend_t;

typedef enum {
	Z_BACKEND_TYPE_EPOLL,
	Z_BACKEND_TYPE_LIBEVENT,
	Z_BACKEND_TYPE_LIBMICROHTTPD
} z_backend_type;

struct z_backend_st {
	/* Private data - Do NOT touch this */
	void *priv;

	/* PUBLIC API */
	void (* set_listening_socket_fd) (struct z_backend_st *, int);
	int  (* run) (struct z_backend_st *);
};

z_backend_t *z_backend_new(z_backend_type);
void         z_backend_destroy(z_backend_t **);

#endif
