/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <errno.h>
#include "errors.h"

enum priority {
	ERROR, CRIT
};

static const char *get_errno_description(int err)
{
	if (E_STR(err)[0] == 'E')
		return E_STR(err);
	else
		return "<unknown>";
}

static const char *get_error_description(int err)
{
	if (strncmp(E_STR(err), "E_", 2) == 0)
		return E_STR(err);
	else
		return "<unknown>";
}

static void assert_print(enum priority prio, const char *header,
		int code, const char *code_desc,
		const char *msg, va_list args)
{
	char *prio_str = "";

	switch (prio) {
	case ERROR:
		prio_str = "ERROR";
		break;
	case CRIT:
		prio_str = "CRIT";
		break;
	}

//	if (!code_desc)
//		printf("%s: %s - %s\n", prio_str, header, msg, args);
//	else
//		printf("%s: %s - %s (%s, %d)\n", prio_str, header, msg, args, code_desc, code);
}

void z_assert(const char *file, int line, const char *msg, ...)
{
	va_list args;
	char file_and_line[50];

	va_start(args, msg);
	snprintf(file_and_line, sizeof(file_and_line), "%s:%d", file, line);
	assert_print(ERROR, file_and_line, 0, NULL, msg, args);
	va_end(args);
}

void z_assert_syscall(const char *file, int line, const char *msg, ...)
{
	va_list args;
	int val = errno;
	char file_and_line[50];
	const char *val_desc = get_errno_description(val);

	va_start(args, msg);
	snprintf(file_and_line, sizeof(file_and_line), "%s:%d", file, line);
	assert_print(ERROR, file_and_line, val, val_desc, msg, args);
	va_end(args);
}

int z_assert_value(const char *file, int line, int val)
{
	char file_and_line[50];

	snprintf(file_and_line, sizeof(file_and_line), "%s:%d", file, line);
//	assert_print(ERROR, file_and_line, val, NULL, "ERROR code: %d", val);

	return val;
}

int z_assert_value2(const char *file, int line, int val, const char *msg, ...)
{
	va_list args;
	char file_and_line[50];
	const char *val_desc = get_error_description(val);

	va_start(args, msg);
	snprintf(file_and_line, sizeof(file_and_line), "%s:%d", file, line);
	assert_print(ERROR, file_and_line, val, val_desc, msg, args);
	va_end(args);

	return val;
}

void z_crit(const char *file, int line, const char *msg, ...)
{
	va_list args;
	char file_and_line[50];

	va_start(args, msg);
	snprintf(file_and_line, sizeof(file_and_line), "%s:%d", file, line);
	assert_print(CRIT, file_and_line, 0, NULL, msg, args);
	va_end(args);
}
