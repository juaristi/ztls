/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include <errno.h>
#include <pthread.h>
#include <event2/event.h>
#include <picotls.h>
#include "mm.h"
#include "tree.h"
#include "errors.h"
#include "socket.h"
#include "handler.h"
#include "z-assert.h"

z_tree_t *tree;

struct z_libevent_backend_priv {
	int accept_fd;
};

struct ev_priv {
	struct event  *ev;
	struct event_base *evbase;
};

static int monitor_new_fd(struct event_base *evbase, int fd, socket_type_t type);
static void _socket_read_event_cb(evutil_socket_t fd, short events, void *priv);

static void handle_signal(int sig, struct event_base *evbase, pthread_t tid)
{
	if (sig == SIGTERM || sig == SIGINT) {
		if (event_base_loopbreak(evbase) == -1) {
			Z_ASSERT("event_base_loopbreak() failed");
			pthread_cancel(tid);
		}
	}
}

static socket_state_t handle_input(z_socket_t *s)
{
	int retval;
	char in[1024], *out;
	size_t outlen;
	ssize_t read_bytes;
	ptls_buffer_t sndbuf;
	ptls_handshake_properties_t props;

	read_bytes = read(s->fd, in, sizeof(in));

	if (read_bytes <= 0) {
		if (errno == EINTR)
			s->state |= INTERRUPTED;
		else
			s->state = FAILED;
		goto end;
	}

	if ((s->state & RECV_CLIENT_HELLO) == RECV_CLIENT_HELLO) {
		retval = ptls_handshake(s->ptls, &sndbuf, in, &read_bytes, NULL);

		if (retval == PTLS_ERROR_IN_PROGRESS)
			s->state = OPAQUE_IN_PROGRESS;
		else if (retval == 0)
			s->state = FINISHED;
		else
			s->state = FAILED;

		z_write(s, sndbuf.base, sndbuf.off);
	}

end:
	return s->state;
}

static void _socket_read_event_cb(evutil_socket_t fd, short events, void *priv)
{
	int new_fd;
	z_socket_t *s;
	struct ev_priv *p = priv;

	/*
	 * Basic sanity checks.
	 * If we don't pass these, remove this event and the socket entry,
	 * as it's obvious something's broken.
	 */
	if (!p || !p->ev || !p->evbase) {
		Z_ASSERT_value(E_INTERNAL_ERROR);
		goto remove_socket_entry;
	}

	if ((events & EV_SIGNAL) == EV_SIGNAL) {
		handle_signal(fd, p->evbase, pthread_self());
		return;
	}

	/*
	 * (More) basic sanity checks.
	 * In this case, it could also be that the socket entry 's' timed out and was removed from the tree.
	 * But in either case we close the socket and remove the event.
	 */
	if (!s || s->fd != fd) {
		Z_ASSERT_value(E_INTERNAL_ERROR);
		goto remove_socket_entry;
	}

	s = tree->get(tree, fd);

	switch (s->type) {
	case ACCEPT_FD:
		/* New connection - Accept it and add it to the event loop */
		new_fd = accept(s->fd, NULL, NULL);

		if (new_fd == -1) {
			Z_ASSERT_syscall("child: accept() failed");
		} else {
			if (monitor_new_fd(p->evbase, new_fd, CLIENT_SOCKET_FD) < 0)
				Z_ASSERT("child: Could not monitor new socket");
		}
		break;
	case CLIENT_SOCKET_FD:
		if ((events & EV_WRITE) == EV_WRITE)
			z_try_flush(s);

		if ((events & EV_READ) == EV_READ) {
			if (handle_input(s) == FINISHED)
				goto remove_socket_entry;
		}

		break;
	default:
		Z_ASSERT_value2(E_INTERNAL_ERROR, "Unknown socket type (%d)", s->type);
		goto remove_socket_entry;
	}

	return;

remove_socket_entry:
	close(fd);

	if (p->ev)
		event_del(p->ev);

	if (tree->remove(tree, fd) != 0 && s != NULL)
		Z_CRIT("BUG !!! THIS WILL PROBABLY LEAK MEMORY. PLEASE RESTART THE SERVER.");
}

static int monitor_new_fd(struct event_base *evbase, int fd, socket_type_t type)
{
	int retval;
	z_socket_t *s;
	struct event *ev = event_new(evbase, fd,
			EV_READ | EV_WRITE | EV_ET | EV_PERSIST,
			_socket_read_event_cb,
			event_self_cbarg());

	if (!ev) {
		Z_ASSERT_value2(E_INTERNAL_ERROR, "Could not create socket event (socket FD: %d)", fd);
		return -1;
	}

	s = mm_new0(z_socket_t);
	s->fd = fd;
	s->type = type;

	if ((retval = tree->add(tree, fd, s)) < 0) {
		event_free(ev);
		Z_ASSERT_value(retval);
		return -1;
	}

	if (event_add(ev, NULL) == -1) {
		event_free(ev);
		tree->remove(tree, fd);

		Z_ASSERT_value2(E_INTERNAL_ERROR, "Could not add event to queue (socket FD: %d)", fd);

		return -1;
	}

	return 0;
}

static int monitor_signals(struct event_base *evbase,
		struct event **ev_out_1,
		struct event **ev_out_2)
{
	*ev_out_1 = NULL;
	*ev_out_2 = NULL;

	*ev_out_1 = evsignal_new(evbase, SIGTERM,
			_socket_read_event_cb,
			event_self_cbarg());
	if (!*ev_out_1) {
		Z_ASSERT_value2(E_INTERNAL_ERROR, "Could not create signal event");
		goto bail;
	}
	if (evsignal_add(*ev_out_1, NULL) == -1) {
		Z_ASSERT_value2(E_INTERNAL_ERROR, "Could not add event to queue");
		goto bail;
	}

	*ev_out_2 = evsignal_new(evbase, SIGINT,
			_socket_read_event_cb,
			event_self_cbarg());
	if (!*ev_out_2) {
		Z_ASSERT_value2(E_INTERNAL_ERROR, "Could not create signal event");
		goto bail;
	}
	if (evsignal_add(*ev_out_2, NULL) == -1) {
		Z_ASSERT_value2(E_INTERNAL_ERROR, "Could not add event to queue");
		goto bail;
	}

	return 0;


bail:
	if (*ev_out_1) {
		evsignal_del(*ev_out_1);
		event_free(*ev_out_1);
		*ev_out_1 = NULL;
	}

	if (*ev_out_2) {
		evsignal_del(*ev_out_2);
		event_free(*ev_out_2);
		*ev_out_2 = NULL;
	}

	return -1;
}

static int _run(int accept_fd)
{
	int retval = 0;
	struct sigaction sa;
	struct event_base *evbase;
	struct event *signal_evs[2] = { NULL, NULL };

	/* Mask all signals */
	sigemptyset(&sa.sa_mask);
	sa.sa_flags = 0;
	sa.sa_handler = SIG_DFL;

	sigaction(SIGINT, &sa, NULL);
	sigaction(SIGTERM, &sa, NULL);

#ifdef EVTHREAD_USE_PTHREADS_IMPLEMENTED
	if (evthread_use_pthreads() == -1) {
		Z_ASSERT_value2(E_INTERNAL_ERROR, "Could not set up libevent pthreads");
		goto end;
	}
#endif

	/* TODO override libevent's log functions */

	evbase = event_base_new();
	if (!evbase) {
		Z_ASSERT_value2(E_INTERNAL_ERROR, "Could not create event base");
		retval = 1;
		goto end;
	}

	/* Add interesting signals to the loop */
	if (monitor_signals(evbase, &signal_evs[0], &signal_evs[1]) < 0) {
		Z_ASSERT("child: Could not add signals to event queue");
		retval = 1;
		goto end;
	}

	/* Add the parent's IPC socket to the loop */
	if (monitor_new_fd(evbase, accept_fd, ACCEPT_FD) < 0) {
		Z_ASSERT("child: Could not add listening socket to event queue");
		retval = 1;
		goto end;
	}

	/* Run the loop! */
	retval = event_base_loop(evbase, EVLOOP_NO_EXIT_ON_EMPTY);

	if (retval == -1) {
		Z_ASSERT("Event base loop failed");
		retval = 1;
	}

end:
	if (signal_evs[0]) {
		evsignal_del(signal_evs[0]);
		event_free(signal_evs[0]);
	}
	if (signal_evs[1]) {
		evsignal_del(signal_evs[1]);
		event_free(signal_evs[1]);
	}

	event_base_free(evbase);
	return retval;
}

static int run(z_backend_t *be)
{
	struct z_libevent_backend_priv *priv;

	if (!be || !be->priv)
		return Z_ASSERT_value(E_INVALID_ARGUMENT);

	priv = be->priv;
	if (priv->accept_fd <= 0)
		return Z_ASSERT_value(E_INVALID_VALUE);

	return _run(priv->accept_fd);
}

static void set_listening_socket_fd(z_backend_t *be, int accept_fd)
{
	if (be && be->priv && accept_fd > 0)
		((struct z_libevent_backend_priv *) be->priv)->accept_fd = accept_fd;
}

z_backend_t *z_backend_new(z_backend_type type)
{
	z_backend_t *be;
	struct z_libevent_backend_priv *priv;

	if (type != Z_BACKEND_TYPE_LIBEVENT) {
		Z_ASSERT("Backend type %d is not implemented", type);
		return NULL;
	}

	be = mm_new0(z_backend_t);
	if (!be) {
		Z_ASSERT_value(E_MEMORY_ERROR);
		return NULL;
	}

	priv = mm_new0(struct z_libevent_backend_priv);
	if (!priv) {
		free(be);
		Z_ASSERT_value(E_MEMORY_ERROR);
		return NULL;
	}

	be->priv = priv;
	memset(priv, 0, sizeof(struct z_libevent_backend_priv));

	be->run = run;
	be->set_listening_socket_fd = set_listening_socket_fd;
	return be;
}

void z_backend_destroy(z_backend_t **be)
{
	if (be && *be) {
		if ((*be)->priv)
			free((*be)->priv);
		free(*be);
		*be = NULL;
	}
}
