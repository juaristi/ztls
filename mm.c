/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include <string.h>
#include "mm.h"

#include "z-assert.h"

void mm_strdup(char **dst, const char *src)
{
	if (dst) {
		if (*dst)
			mm_free(*dst);
		if (src)
			*dst = strdup(src);
	}
}

void *mm_malloc0(size_t len)
{
	void *m = calloc(1, len);
	if (!m)
		Z_CRIT("Out of memory");
	return m;
}
