/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include <errno.h>
#include <sys/socket.h>
#include "socket.h"

void z_append(z_socket_t *s, const uint8_t *data, size_t datalen)
{
	/* TODO implement this */
}

void z_try_flush(z_socket_t *s)
{
	int bytes_sent;

	if ((s->state & WRITE_PENDING) == WRITE_PENDING) {
		bytes_sent = send(s->fd, s->out, s->outlen, MSG_DONTWAIT);

		if (bytes_sent == s->outlen) {
			/* Remove the WRITE_PENDING flag */
			s->state ^= WRITE_PENDING;
			return;
		}

		if (bytes_sent == -1) {
			if (errno == EINTR)
				s->state |= INTERRUPTED;
			else if (errno != EWOULDBLOCK)
				s->state = FAILED;
		}
	}
}

void z_write(z_socket_t *s, const uint8_t *data, size_t datalen)
{
	int bytes_sent;

	if ((s->state & WRITE_PENDING) == WRITE_PENDING)
		z_append(s, data, datalen);

	bytes_sent = send(s->fd, s->out, s->outlen, MSG_DONTWAIT);

	if (bytes_sent == s->outlen) {
		/* Remove the WRITE_PENDING flag */
		s->state ^= WRITE_PENDING;
		return;
	}

	if (bytes_sent == -1) {
		switch (errno) {
		case EWOULDBLOCK:
			/* Set the WRITE_PENDING flag */
			s->state |= WRITE_PENDING;
			break;
		case EINTR:
			s->state |= INTERRUPTED;
			break;
		default:
			s->state = FAILED;
			break;
		}

		return;
	}

	/* Set the WRITE_PENDING flag */
	s->state |= WRITE_PENDING;
}
