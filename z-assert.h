/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef _ASSERT_H_
#define _ASSERT_H_

#define Z_PRINTF_STYLE(x, y) __attribute__((format(printf, x, y)))

#define Z_ASSERT(...) z_assert(__FILE__, __LINE__, __VA_ARGS__)
#define Z_ASSERT_syscall(...) z_assert_syscall(__FILE__, __LINE__, __VA_ARGS__)

void z_assert(const char *, int, const char *, ...) Z_PRINTF_STYLE(3, 4);
void z_assert_syscall(const char *, int, const char *, ...) Z_PRINTF_STYLE(3, 4);

#define Z_ASSERT_value(v) z_assert_value(__FILE__, __LINE__, v)
#define Z_ASSERT_value2(v, ...) z_assert_value2(__FILE__, __LINE__, v, __VA_ARGS__)

int z_assert_value(const char *, int, int);
int z_assert_value2(const char *, int, int,
		const char *, ...) Z_PRINTF_STYLE(4, 5);

#define Z_CRIT(...) z_crit(__FILE__, __LINE__, __VA_ARGS__)
void z_crit(const char *, int, const char *, ...) Z_PRINTF_STYLE(3, 4);

#endif
