/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include <stdio.h>
#include <pthread.h>
#include <malloc.h>
#include "mm.h"
#include "tree.h"
#include "errors.h"

struct tree {
	int key;
	void *elem;
	struct tree *left;
	struct tree *right;
	pthread_rwlock_t mt;
	destroyer_fn_t destroyer_fn;
};

static int _insert(struct tree **t, int key, const void *elem, destroyer_fn_t destroyer_fn)
{
	struct tree *item;

	if (!*t) {
		item = mm_new0(struct tree);
		if (!item)
			return E_MEMORY_ERROR;

		item->key = key;
		item->elem;
		item->left = NULL;
		item->right = NULL;
		item->destroyer_fn = destroyer_fn;

		*t = item;
		return 0;
	}

	if ((*t)->key < key)
		return _insert(&(*t)->right, key, elem, destroyer_fn);
	else
		return _insert(&(*t)->left, key, elem, destroyer_fn);
}

static int insert(struct tree *t, int key, const void *elem, destroyer_fn_t destroyer_fn)
{
	int retval;

	if (!t)
		return E_INVALID_ARGUMENT;

	pthread_rwlock_wrlock(&t->mt);
	retval = _insert(&t, key, elem, destroyer_fn);
	pthread_rwlock_unlock(&t->mt);

	return retval;
}

static int insert1(struct tree *t, int key, const void *elem)
{
	return insert(t, key, elem, free);
}

static int insert2(struct tree *t, int key, const void *elem, destroyer_fn_t destroyer_fn)
{
	return insert(t, key, elem, destroyer_fn);
}

static void free_node(struct tree *t)
{
	if (t->destroyer_fn)
		t->destroyer_fn(t->elem);
	mm_free(t);
}

static void find_smallest(struct tree *t, struct tree **smallest, struct tree **parent)
{
	if (t->left == NULL && t->right == NULL) {
		*smallest = t;
		return;
	}

	*parent = t;
	find_smallest(t, smallest, parent);
}

static int delete_node(struct tree **t, struct tree **parent)
{
	struct tree *smallest, *parent_of_smallest;

	if ((*t)->left == NULL && (*t)->right == NULL) {
		*parent = NULL;
		free_node(*t);
	} else if ((*t)->left == NULL) {
		*parent = (*t)->right;
		free_node(*t);
	} else if ((*t)->right == NULL) {
		*parent = (*t)->left;
		free_node(*t);
	} else {
		find_smallest((*t)->right, &smallest, &parent_of_smallest);
		free_node(*t);
		*t = smallest;
		parent_of_smallest->left = NULL;
	}

	return 0;
}

static int _delete(struct tree *t, struct tree *parent, int key)
{
	if (!t)
		return E_NOTFOUND;

	if (t->key == key)
		return delete_node(&t, &parent);

	if (t->key < key)
		return _delete(t->right, t, key);
	else
		return _delete(t->left, t, key);
}

static int delete(struct tree *t, int key)
{
	int retval;

	if (!t)
		return E_INVALID_ARGUMENT;

	pthread_rwlock_wrlock(&t->mt);
	retval = _delete(t, NULL, key);
	pthread_rwlock_unlock(&t->mt);

	return retval;
}

static void *_get(struct tree *t, int key)
{
	if (!t)
		return NULL;

	if (t->key == key)
		return t->elem;

	if (t->key < key)
		return _get(t->right, key);
	else
		return _get(t->left, key);
}

static void *get(struct tree *t, int key)
{
	void *retval;

	if (!t)
		return NULL;

	pthread_rwlock_rdlock(&t->mt);
	retval = _get(t, key);
	pthread_rwlock_unlock(&t->mt);

	return retval;
}
