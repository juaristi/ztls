/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef _CONFIG_FILE_H_
#define _CONFIG_FILE_H_
#include <unistd.h>
#include <netinet/in.h>

struct z_config_st;
typedef struct z_config_st z_config_t;

struct z_config_st {
	int (* get_uid) (z_config_t *, uid_t *);
	int (* get_gid) (z_config_t *, gid_t *);
	int (* set_uid) (z_config_t *, uid_t);
	int (* set_gid) (z_config_t *, gid_t);
	int (* get_backlog) (z_config_t *);
	int (* get_listen_addr) (z_config_t *,
			in_addr_t *,
			in_port_t *);
	int (* set_listen_addr) (z_config_t *,
			long,
			int);

	/* Private data - Do NOT touch this pointer */
	void *priv;
};

int  z_config_init(z_config_t **, const char *);
int  z_config_reload(z_config_t *, const char *);
void z_config_deinit(z_config_t **);

#endif
