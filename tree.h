/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef _TREE_H_
#define _TREE_H_

typedef void (* destroyer_fn_t) (void *);

struct z_tree_st;
typedef struct z_tree_st z_tree_t;

struct z_tree_st {
	/* Private data - Do NOT touch this pointer! */
	void *priv;

	int (* add) (z_tree_t *, int, const void *);
	int (* add2)(z_tree_t *, int, const void *, destroyer_fn_t);
	int (* remove) (z_tree_t *, int);
	void * (* get) (z_tree_t *, int);
};

#endif
