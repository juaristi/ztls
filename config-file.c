/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <unistd.h>
#include <stdbool.h>
#include <libconfig.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include "mm.h"
#include "errors.h"
#include "config-file.h"
#include "z-assert.h"

struct config_pr {
	char *config_file;

	long int
		uid,
		gid;

#define DEFAULT_PORT 443
	in_addr_t addr;
	in_port_t port;
	bool implicit_port;

#define DEFAULT_BACKLOG 5
	int backlog;

	struct {
		bool
			uid,
			gid,
			addr,
			port;
	} overrides;
};

static long int read_third_column_as_int(const char *filename, const char *name)
{
	FILE *fp;
	bool user_found = false;
	long int result = E_NOTFOUND;
	char buf[1024], *ptr, *ptr2, *ptr3;
	int buflimit = sizeof(buf) - 1;

	buf[buflimit] = 0;

	fp = fopen(filename, "r");
	if (!fp)
		return E_IO_ERROR;

	/* Find the line where the first column equals 'name' */
	while (!feof(fp) && !user_found) {
		if (!fgets(buf, buflimit, fp))
			break;
		if (strncmp(buf, name, buflimit) == 0)
			user_found = true;
	}

	if (!user_found)
		goto end;

	/* We found the line - Now get to the third column, and convert to long int */
	if ((ptr = strchr(buf, ':')) &&
			(ptr = strchr(++ptr, ':')) &&
			(ptr2 = strchr(++ptr, ':'))) {
		result = strtol(ptr, &ptr3, 10);
		if (result < INT_MIN || result > INT_MAX)
			result = E_OUT_OF_BOUNDS;
		if (ptr3 != ptr2 || result < 0)
			result = E_INTERNAL_ERROR;
	} else {
		result = E_TOOBIG;
	}

end:
	fclose(fp);
	return result;
}

static int find_user(const char *username, long int *uid)
{
	long int retval = read_third_column_as_int("/etc/passwd", username);

	if (retval > 0) {
		*uid = retval;
		retval = 0;
	}

	return retval;
}

static int find_group(const char *groupname, long int *gid)
{
	long int retval = read_third_column_as_int("/etc/group", groupname);

	if (retval > 0) {
		*gid = retval;
		retval = 0;
	}

	return retval;
}

static int parse_port(const char *port, struct config_pr *p)
{
	long int result;
	char *endptr = NULL;

	result = strtol(port, &endptr, 10);
	if (result < 0 || endptr == port)
		return Z_ASSERT_value(E_INVALID_VALUE);
	if (result > USHRT_MAX)
		return Z_ASSERT_value(E_OUT_OF_BOUNDS);

	p->port = result;
	return 0;
}

static int parse_addr(const char *addr, struct config_pr *p)
{
	int ret;
	char *ptr;
	struct in_addr in_addr;

	if ((ptr = strchr(addr, ':'))) {
		if ((ret = parse_port(++ptr, p)) < 0)
			return Z_ASSERT_value(ret);
		p->implicit_port = true;
	}

	/* TODO switch to getaddrinfo(3) when we support IPv6 */
	if (!inet_aton(addr, &in_addr)) {
		p->port = 0;
		p->implicit_port = false;
		return Z_ASSERT_value(E_IO_ERROR);
	}

	p->addr = in_addr.s_addr;
	return 0;
}

static int load_config(struct config_pr *p)
{
	int ret;
	uid_t uid;
	gid_t gid;
	config_t config;
	const char *username = NULL, *groupname = NULL, *addr = NULL;

	config_init(&config);

	/* Parse config file */
	if (config_read_file(&config, p->config_file) != CONFIG_TRUE) {
		ret = Z_ASSERT_value(E_IO_ERROR);
		goto end;
	}

	/* Read 'user' and 'group' */
	if (config_lookup_string(&config, "user", &username) != CONFIG_TRUE ||
			find_user(username, &uid) < 0) {
		uid = getuid();
		printf("'user' configuration value not found. Falling back to UID %ld\n", p->uid);
	}
	if (config_lookup_string(&config, "group", &groupname) != CONFIG_TRUE ||
			find_group(groupname, &gid) < 0) {
		gid = getgid();
		printf("'group' configuration value not found. Falling back to GID %ld\n", p->gid);
	}

	if (!p->overrides.uid)
		p->uid = uid;
	if (!p->overrides.gid)
		p->gid = gid;

	/* Read listening address and port, and backlog */
	if (config_lookup_int(&config, "listen.backlog", &p->backlog) != CONFIG_TRUE) {
		p->backlog = DEFAULT_BACKLOG;
		printf("listen.backlog configuration value not found."
				" Falling back to default backlog: %d\n", p->backlog);
	}
	if (!p->overrides.addr &&
		(config_lookup_string(&config, "listen.addr", &addr) != CONFIG_TRUE ||
			parse_addr(addr, p) < 0)) {
		p->addr = 0;
		printf("listen.addr configuration value not found."
				" Falling back to default address: 0.0.0.0\n");
	}
	if (!p->overrides.port &&
		(!p->implicit_port && (config_lookup_string(&config, "listen.port", &addr) != CONFIG_TRUE ||
			parse_port(addr, p) < 0))) {
		p->port = DEFAULT_PORT;
		printf("listen.port configuration value not found."
				" Falling back to default port: %d\n", p->port);
	}

	ret = 0;

end:
	config_destroy(&config);
	return ret;
}

static int get_uid(z_config_t *c, uid_t *uid)
{
	struct config_pr *p;

	if (!c || !c->priv || !uid)
		return Z_ASSERT_value(E_INVALID_ARGUMENT);

	p = c->priv;

	if (p->uid < 0 || p->uid > INT_MAX)
		return Z_ASSERT_value(E_INTERNAL_ERROR);

	*uid = (uid_t) p->uid;
	return 0;
}

static int get_gid(z_config_t *c, gid_t *gid)
{
	struct config_pr *p;

	if (!c || !c->priv || !gid)
		return Z_ASSERT_value(E_INVALID_ARGUMENT);

	p = c->priv;

	if (p->gid < 0 || p->gid > INT_MAX)
		return Z_ASSERT_value(E_INTERNAL_ERROR);

	*gid = (gid_t) p->gid;
	return 0;
}

static int set_uid(z_config_t *c, uid_t uid)
{
	struct config_pr *p;

	if (!c || !c->priv)
		return Z_ASSERT_value(E_INVALID_ARGUMENT);

	p = c->priv;

	p->uid = uid;
	p->overrides.uid = true;
	return 0;
}

static int set_gid(z_config_t *c, gid_t gid)
{
	struct config_pr *p;

	if (!c || !c->priv)
		return Z_ASSERT_value(E_INVALID_ARGUMENT);

	p = c->priv;

	p->gid = gid;
	p->overrides.gid = true;
	return 0;
}

static int get_backlog(z_config_t *c)
{
	if (!c || !c->priv)
		return Z_ASSERT_value(E_INVALID_ARGUMENT);

	return ((struct config_pr *) c->priv)->backlog;
}

static int get_listen_addr(z_config_t *c, in_addr_t *addr, in_port_t *port)
{
	struct config_pr *p;

	if (!c || !c->priv)
		return Z_ASSERT_value(E_INVALID_ARGUMENT);

	p = c->priv;

	if (addr)
		*addr = p->addr;
	if (port)
		*port = p->port;

	return 0;
}

static int set_listen_addr(z_config_t *c, long addr, int port)
{
	struct config_pr *p;

	if (!c || !c->priv)
		return Z_ASSERT_value(E_INVALID_ARGUMENT);

	p = c->priv;

	if (addr > UINT32_MAX)
		return Z_ASSERT_value(E_INVALID_VALUE);
	if (port > UINT16_MAX)
		return Z_ASSERT_value(E_INVALID_VALUE);

	if (addr >= 0) {
		p->addr = (in_addr_t) addr;
		p->overrides.addr = true;
	}
	if (port >= 0) {
		p->port = (in_port_t) port;
		p->overrides.port = true;
	}

	return 0;
}

/**
 * \param[in] c
 * \param[in] config_file
 * \return
 *
 * If \p config_file is NULL, a \p z_config_t object
 * will be created, but it won't be populated. Hence, all the subsequent
 * calls to its getter functions will return `E_NOTFOUND`.
 */
int z_config_init(z_config_t **c, const char *config_file)
{
	int retval = E_MEMORY_ERROR;
	struct config_pr *p;

	if (!c)
		return E_INVALID_ARGUMENT;

	*c = mm_new0(z_config_t);
	if (!*c)
		goto bail;

	p = mm_new0(struct config_pr);
	if (!p)
		goto bail;

	mm_strdup(&p->config_file, config_file);

	retval = load_config(p);

	if (retval < 0)
		goto bail;

	(*c)->priv = p;
	(*c)->get_backlog = get_backlog;
	(*c)->get_listen_addr = get_listen_addr;
	(*c)->set_listen_addr = set_listen_addr;
	(*c)->get_uid = get_uid;
	(*c)->get_gid = get_gid;
	(*c)->set_uid = set_uid;
	(*c)->set_gid = set_gid;

	return retval;

bail:
	if (p)
		mm_free(p);
	MM_FREE2(c);
	return retval;
}

/**
 * \param[in] c
 * \param[in] config_file
 * \return
 *
 * If \p config_file is NULL, an attempt will be made to read
 * the file that was supplied when z_config_init() was called.
 * If no config file was specified in z_config_init() either, then this function
 * will fail with `E_INVALID_ARGUMENT`.
 */
int z_config_reload(z_config_t *c, const char *config_file)
{
	struct config_pr *p;

	if (!c || !c->priv)
		return E_INVALID_ARGUMENT;

	p = c->priv;

	if (config_file)
		mm_strdup(&p->config_file, config_file);

	return load_config(p);
}

/**
 * \param[in] c
 */
void z_config_deinit(z_config_t **c)
{
	if (c && *c) {
		if ((*c)->priv) {
			mm_free(((struct config_pr *) (*c)->priv)->config_file);
			mm_free((*c)->priv);
		}
		MM_FREE2(c);
	}
}
