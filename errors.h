/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#ifndef _ERRORS_H_
#define _ERRORS_H_

#define E_INTERNAL_ERROR	-1
#define E_IO_ERROR		-2
#define E_NOTFOUND		-3
#define E_OUT_OF_BOUNDS		-4
#define E_TOOBIG		-5
#define E_INVALID_ARGUMENT	-6
#define E_INVALID_VALUE		-7
#define E_MEMORY_ERROR		-8

#define E_STR(e) #e

#endif
