/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <errno.h>
#include <fcntl.h>
#include <signal.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#ifdef HAVE_LIBPCAP
#include <pcap/pcap.h>
#endif
#include "handler.h"
#include "config-file.h"
#include "z-assert.h"

static int read_iface(const char *iface_name, struct sockaddr_in *ipv4_addr)
{
	int retval = -1;
#ifdef HAVE_LIBPCAP
	char errbuf[PCAP_ERRBUF_SIZE];
	pcap_if_t *ifaces = NULL, *iface = NULL;

	if (pcap_findalldevs(&ifaces, errbuf) != 0)
		return Z_ASSERT_value(-1, "pcap_findalldevs() failed");

	for (iface = ifaces; iface && retva; iface = iface->next) {
		if (!strcmp(iface->name, iface_name)) {
			if (iface->addresses == NULL) {
				retval = Z_ASSERT_value(-1, "Device has no addresses");
				break;
			}

			for (cur_addr = iface->addresses; cur_addr; cur_addr = cur_addr->next) {
				if (cur_addr->addr->sin_family == AF_INET) {
					memcpy(ipv4_addr,
						iface->addresses->addr,
						sizeof(struct sockaddr_in));
					retval = 0;
					break;
				}
			}
		}
	}

	pcap_freealldevs(ifaces);
#else
#endif
	return retval;
}

static int create_listening_socket(in_addr_t ipv4_addr, in_port_t port, int backlog)
{
	int ret, sockfd, flags, one = 1;
	struct sockaddr_in addr;
	socklen_t addrlen = sizeof(struct sockaddr);

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1) {
		Z_ASSERT_syscall("socket() failed");
		goto end;
	}

	memset(&addr, 0, sizeof(struct sockaddr_in));
	addr.sin_family = AF_INET;
	addr.sin_port = port;
	addr.sin_addr.s_addr = ipv4_addr;

	setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &one, sizeof(one));

	if (bind(sockfd, (struct sockaddr *) &addr, addrlen) == -1) {
		Z_ASSERT_syscall("Could not bind to address %s:%u",
				inet_ntoa(addr.sin_addr), port);
		close(sockfd);
		sockfd = -1;
		goto end;
	}

	if (listen(sockfd, backlog) == -1) {
		Z_ASSERT_syscall("listen() failed");
		close(sockfd);
		sockfd = -1;
	}

	/* Set socket in non-blocking mode */
	if ((flags = fcntl(sockfd, F_GETFL, 0)) == -1 ||
			fcntl(sockfd, F_SETFL, flags | O_NONBLOCK) == -1) {
		Z_ASSERT("Could not set socket in non-blocking mode");
		close(sockfd);
		sockfd = -1;
	}

end:
	return sockfd;
}

static int run_child(uid_t uid, gid_t gid,
		sigset_t *org_sset, int accept_fd)
{
	int retval;
	z_backend_t *backend;

	/*
	 * We are the child.
	 * Restore default signal mask and drop credentials.
	 */
	if (sigprocmask(SIG_SETMASK, org_sset, NULL) == -1) {
		Z_ASSERT_syscall("child: sigprocmask() failed");
		return 3;
	}

	/* Drop credentials */
	if (setuid(uid) == -1) {
		Z_ASSERT("Could not change user ID to %d", uid);
		return 3;
	}
	if (setgid(gid) == -1) {
		Z_ASSERT("Could not change group ID to %d", gid);
		return 3;
	}

	if (getuid() == 0 || getgid() == 0)
		Z_ASSERT("Server is running as root!");

	/* Run it! */
	backend = z_backend_new(Z_BACKEND_TYPE_LIBEVENT);
	if (backend) {
		backend->set_listening_socket_fd(backend, accept_fd);
		retval = backend->run(backend);
		z_backend_destroy(&backend);
	} else {
		retval = 3;
	}

	close(accept_fd);
	return retval;
}

static int run_parent(uid_t uid, gid_t gid,
		sigset_t *sset, sigset_t *org_sset,
		int accept_fd)
{
	pid_t child_pid;
	int child_status, sig, retval = 0;

	/* Fork a child process and run the server */
	child_pid = fork();

	if (child_pid == -1) {
		Z_ASSERT_syscall("fork() failed");
		close(accept_fd);
		return 2;
	}

	if (child_pid == 0) {
		/* We are the child */
		_exit(run_child(uid, gid, org_sset, accept_fd));
	}

	/* We are the parent */
	close(accept_fd);

	/* Sleep until a signal comes */
	sig = sigwaitinfo(sset, NULL);

	if (sig == -1) {
		Z_ASSERT_syscall("parent: sigwaitinfo() failed");
		kill(child_pid, SIGTERM);
		retval = 1;
	} else if (sig != SIGCHLD) {
		kill(child_pid, SIGTERM);
	}

	waitpid(child_pid, &child_status, 0);

	if (sig == SIGCHLD && WIFSIGNALED(child_status)) {
		Z_ASSERT("parent: Child died. Rebooting.");
		return -1;
	}

	return retval;
}

static int run_server(z_config_t *conf)
{
	uid_t uid;
	gid_t gid;
	in_port_t port;
	struct in_addr ipv4_addr;

	/* Read configuration */
	if (conf->run_as_user(conf, &uid) < 0) {
		z_config_deinit(&conf);
		return 1;
	}
	if (conf->run_as_group(conf, &gid) < 0) {
		z_config_deinit(&conf);
		return 1;
	}
	if ((backlog = conf->get_backlog(conf)) < 0) {
		z_config_deinit(&conf);
		return 1;
	}
	if (conf->get_listen_addr(conf, &ipv4_addr.s_addr, &port) < 0) {
		Z_ASSERT("Could not get listening address");
		z_config_deinit(&conf);
		return 1;
	}

	z_config_deinit(&conf);

	/* Establish our signal dispositions */
	sigemptyset(&sset);
	sigaddset(&sset, SIGINT);
	sigaddset(&sset, SIGHUP);
	sigaddset(&sset, SIGQUIT);
	sigaddset(&sset, SIGTERM);
	sigaddset(&sset, SIGCHLD);
	if (sigprocmask(SIG_BLOCK, &sset, &org_sset) == -1) {
		Z_ASSERT_syscall("sigprocmask() failed");
		return 1;
	}

	do {
		/* Create a listening socket */
		accept_fd = create_listening_socket(ipv4_addr.s_addr, port, backlog);
		if (accept_fd < 0) {
			Z_ASSERT("Could not listen at %s:%d\n",
					inet_ntoa(ipv4_addr),
					port);
			return 1;
		}

		/* Negative return value means create a new socket and run again */
		ret = run_parent(uid, gid, &sset, &org_sset, accept_fd, !no_daemonize);
	} while (ret < 0);

	return ret;
}

static int read_cmdline(z_config_t *conf, int argc, char **argv)
{
	int opt;
	struct sockaddr_in ipv4_addr;
	struct option options[] = {
		{ "interface", required_argument, NULL, 'i' },
		{ "ip-address", required_argument, NULL, 'I' },
		{ "port", required_argument, NULL, 'p' },
		{ "config-file", required_argument, NULL, 'f' },
		{ "no-daemonize", no_argument, NULL, 'D' },
		{ "uid", required_argument, NULL, 'u' },
		{ "gid", required_argument, NULL, 'g' },
		{ "debug", optional_argument, NULL, 'd' },
		{ NULL, 0, NULL, NULL }
	};

	/*
	 * -i: Interface
	 * -p: Port
	 * -I: IP address (might include port)
	 * -D: Don't daemonize
	 * -f: Configuration file
	 * -u: UID
	 * -g: GID
	 * -d: Debug level
	 */
	for (;;) {
		opt = getopt_long(argc, argv, "i:p:I:f:u:g:d:D",
				options, NULL);
		if (opt == -1)
			break;

		switch (opt) {
		case 'i':
			if (read_iface(optarg, &ipv4_addr) < 0) {
				fprintf(stderr, "ERROR: Could not read interface '%s'\n",
					optarg);
				return -1;
			} else {
				conf->set_listen_addr(conf, &ipv4_addr, NULL);
			}
			break;
		case 'I':
			conf->set_listen_addr(conf, &ipv4_addr, NULL);
			break;
		case 'p':
			conf->set_listen_addr(conf, NULL, port);
			break;
		case 'D':
			no_daemonize = true;
			break;
		case 'f':
			if (conf->load_config_file(conf, optarg) < 0) {
				fprintf(stderr, "ERROR: Could not read config file '%s'\n",
					optarg);
				return -1;
			}
			break;
		case 'u':
			uid = strtol(optarg, &endptr, 10);
			if (errno == ERANGE || uid < 0) {
				fprintf(stderr, "Invalid UID\n");
				return -1;
			}
			conf->set_uid(conf, uid);
			break;
		case 'g':
			gid = strtol(optarg, &endptr, 10);
			if (errno == ERANGE || gid < 0) {
				fprintf(stderr, "Invalid GID\n");
				return -1;
			}
			conf->set_gid(conf, gid);
			break;
		case 'd':
			/* TODO Implement this */
			break;
		default:
			fprintf(stderr, "Invalid option: %s\n", argv[optind]);
			/* fall through */
		case 'h':
			return 1;
		}
	}
}

int main(int argc, char **argv)
{
	int retval;

	if (z_config_init(&conf) < 0)
		return 2;

	if ((retval = read_cmdline(&conf, argc, argv)) > 0)
		goto usage;
	else if (retval < 0)
		return 2;

	return run_server(&conf);

usage:
	/* TODO complete this - print usage message and exit */
	return 1;
}
